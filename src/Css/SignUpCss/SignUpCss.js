import backgroundImage from "../../assets/backgroundImage.jpg";

// background sign-up
export const backgroundImageStyle = {
  backgroundImage: `url("${backgroundImage}")`,
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
};

import React, { useEffect, useState } from "react";
import { getMovieList } from "../../service/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTab/MovieTab";
import cinema1 from "../../assets/cinema1.jpg";
export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    getMovieList()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {});
  }, []);
  return (
    <div
      style={{
        backgroundImage: `url(${cinema1})`,
      }}
    >
      <div className="container px-10 bg-slate-500 pt-5">
        <MovieList movieArr={movieArr} />
        <MovieTab />
      </div>
    </div>
  );
}

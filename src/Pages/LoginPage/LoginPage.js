import React from "react";
import { Button, Form, Input, message } from "antd";
import { postLogin } from "../../service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userLocalService } from "../../service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/132330-happy-new-year-2023.json";
import cinema from "../../assets/cinemaLogin.jpg";
import { setUserInfo } from "../../redux-toolkit/userSlice";
export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  //Đăng nhập
  const onFinish = (values) => {
    postLogin(values)
      .then((res) => {
        message.success(" Đăng Nhập Thành Công ");
        dispatch(setUserInfo(res.data.content));
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
      });
  };

  const onFinishFailed = (errorInfo) => {};
  return (
    <div
      style={{
        backgroundImage: `url(${cinema})`,
        width: "100vw",
        height: "100vh",
      }}
      className="flex flex-col items-center  justify-center h-screen overflow-hidden text-white bg-slate-400 "
    >
      <div className="container mx-auto w-1/2 text-center p-10 bg-white flex ">
        <div className="w-1/2  ">
          <div className="w-1/2">
            <Lottie animationData={bg_animate} loop={true} />
          </div>
        </div>
        <div className="w-1/2 ">
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              className="text-center"
              wrapperCol={{
                span: 24,
              }}
            >
              <Button
                className="bg-blue-500 hover:text-white"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}

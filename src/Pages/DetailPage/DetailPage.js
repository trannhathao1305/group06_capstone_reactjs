import React, { useEffect } from "react";
import "../../Css/DetailCss/RatingCircle.css";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { getMovieShowtimeInfor } from "../../service/movieService";
import moment from "moment/moment";
import { Rate, Tabs } from "antd";
import TabPane from "antd/es/tabs/TabPane";
export default function DetailPage() {
  const [filmDetail, setFilmDetail] = useState({});
  let params = useParams();
  useEffect(() => {
    let { maPhim } = params;
    getMovieShowtimeInfor(maPhim)
      .then((res) => {
        setFilmDetail(res.data.content);
      })
      .catch((err) => {});
  }, [params]);
  return (
    <div
      style={{
        backgroundImage: `url(${filmDetail.hinhAnh})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        minHeight: "100vh",
      }}
    >
      <div className=" w-full backdrop-blur-sm bg-white/10 pt-32 min-h-screen	">
        <div className="grid grid-cols-12">
          <div className="col-span-5 col-start-3">
            <div className="grid grid-cols-3 text-black font-bold">
              <img
                className="col-span-1 object-cover h-80"
                src={filmDetail.hinhAnh}
                alt="movie"
              />
              <div className="col-span-2 ml-5 mt-14 md:h-full">
                <p className="text-lg font-bold bg-yellow-400 text-center rounded-lg px-2">
                  Ngày chiếu:{" "}
                  {moment(filmDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
                </p>
                <p className="text-4xl text-center text-white pb-1">
                  {filmDetail.tenPhim}
                </p>
                <p className="text-sm text-center backdrop-blur-sm bg-white/10 p-1 rounded hidden sm:block">
                  {filmDetail.moTa}
                </p>
              </div>
            </div>
          </div>
          <div className="col-span-4 space-x-20 ">
            <h1
              style={{
                fontSize: "25px",
                marginLeft: "20%",
                fontWeight: "bold",
                color: "yellow",
              }}
            >
              Đánh giá
            </h1>
            <h1 style={{ marginLeft: "10%" }} className="text-2xl py-2">
              <Rate
                className="backdrop-blur-sm bg-white/10 rounded-lg text-center"
                allowHalf
                defaultValue={filmDetail.danhGia / 2}
                style={{ color: "yellow", fontSize: 30 }}
              />
            </h1>
            <div
              style={{ marginLeft: "18%" }}
              className={`c100 p${filmDetail.danhGia * 10} green`}
            >
              <span>{filmDetail.danhGia * 10}%</span>
              <div className="slice">
                <div className="bar"></div>
                <div className="fill"></div>
              </div>
            </div>
          </div>
        </div>
        <div className="container mx-auto mt-5 bg-white py-3 w-2/3 h-screen">
          <Tabs defaultActiveKey="1" centered>
            <TabPane tab="Lịch chiếu" key="1" style={{ minHeight: 300 }}>
              <div>
                <Tabs tabPosition="left">
                  {filmDetail.heThongRapChieu?.map((heThongRap, index) => {
                    return (
                      <TabPane
                        tab={
                          <div>
                            <img
                              src={heThongRap.logo}
                              width="50"
                              alt={heThongRap.logo}
                              className="rounded-full"
                            />
                            {heThongRap.tenHeThongRap}
                          </div>
                        }
                        key={index}
                      >
                        {heThongRap.cumRapChieu?.map((cumRap, index) => {
                          return (
                            <div className="mt-5" key={index}>
                              <div className="flex flex-row">
                                <img
                                  style={{ width: 60, height: 60 }}
                                  src="https://occ-0-58-64.1.nflxso.net/dnm/api/v6/19OhWN2dO19C9txTON9tvTFtefw/AAAABfpnX3dbgjZ-Je8Ax3xn0kXehZm_5L6-xe6YSTq_ucht9TI5jwDMqusWZKNYT8DfGudD0_wWVVTFLiN2_kaQJumz2iivUWbIbAtF.png?r=11f"
                                  alt="hinhanh"
                                />
                                <div className="ml-2">
                                  <p
                                    style={{
                                      fontSize: 20,
                                      fontWeight: "bold",
                                      lineHeight: 1,
                                    }}
                                  >
                                    {cumRap.tenCumRap}
                                  </p>
                                  <p
                                    className="text-gray-400"
                                    style={{ marginTop: 15 }}
                                  >
                                    {cumRap.diaChi}
                                  </p>
                                </div>
                              </div>
                              <div className="grid grid-cols-4">
                                {cumRap.lichChieuPhim
                                  ?.slice(0, 8)
                                  .map((lichChieu, index) => {
                                    return (
                                      <div key={index} className="col-span-1">
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("DD/MM/YYYY ~ hh:mm A")}
                                      </div>
                                    );
                                  })}
                              </div>
                            </div>
                          );
                        })}
                      </TabPane>
                    );
                  })}
                </Tabs>
              </div>
            </TabPane>
            <TabPane tab="Thông tin" key="2" className="text-center">
              Thông tin
            </TabPane>
            <TabPane tab="Đánh giá" key="3" className="text-center">
              Đánh giá
            </TabPane>
          </Tabs>
        </div>
      </div>
    </div>
  );
}

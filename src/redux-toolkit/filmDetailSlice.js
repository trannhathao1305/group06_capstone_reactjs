import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  filmDetail: {},
};

const filmDetailSlice = createSlice({
  name: "filmDetailSlice",
  initialState,
  reducers: {
    getFilmDetail: (state, action) => {
      state.filmDetail = action.payload;
    },
  },
});

export const { getFilmDetail } = filmDetailSlice.actions;

export default filmDetailSlice.reducer;

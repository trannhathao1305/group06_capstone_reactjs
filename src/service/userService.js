import { https } from "./configURL";

export let postSignUp = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangKy", data);
};

export let postLogin = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangNhap", data);
};
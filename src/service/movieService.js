import { https } from "./configURL";

export const getMovieByTheater = () => {
  return https.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap`);
};

export const getMovieList = () => {
  return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`);
};

export const getMovieTheaterInfo = () => {
  return https.get("/api/QuanLyRap/LayThongTinHeThongRap");
};

export const getMovieShowtimeInfor = (maPhim) => {
  return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`);
};
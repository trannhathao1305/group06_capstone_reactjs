import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });

  const handleLogout = () => {
    userLocalService.remove();
    window.location.reload();
  };
  const renderContent = () => {
    if (user) {
      return (
        <>
          <span className="text-yellow-300 font-semibold">{user?.hoTen}</span>
          <button
            onClick={handleLogout}
            className="border-2 font-medium px-5 py-2 rounded bg-white"
          >
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-2 px-5 py-2 rounded font-medium bg-white"
          >
            Đăng Nhập
          </button>
          <button
            onClick={() => {
              window.location.href = "/sign-up";
            }}
            className="border-2 px-5 py-2 font-medium rounded bg-white"
          >
            Đăng Ký
          </button>
        </>
      );
    }
  };
  return <div className="space-x-3">{renderContent()}</div>;
}

import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="py-5 px-20 flex justify-between items-center shadow-lg bg-black ">
      <NavLink to={"/"}>
        <span className=" text-red-600 text-3xl font-medium">
          MOVIE.CAPSTONE.GROUP6
        </span>
      </NavLink>
      <div className="flex justify-center items-center font-extrabold text-white ">
        <span className="pr-4">
          <a href="#LichChieu">Lịch Chiếu</a>
        </span>
        <span>
          <a href="#CumRap">Cụm Rạp</a>
        </span>
      </div>
      <UserNav />
    </div>
  );
}
